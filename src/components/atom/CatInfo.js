import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const CatInfo = ({species}) => {
    return (
        <View>
            <Text style={styles.catName}>
                Name: {species.name}
            </Text>
            <Text style={styles.catColor}>
                Warna: {species.warna}
            </Text>

        </View>
    )
};

const styles = StyleSheet.create({
    catName:{
        fontSize:30,
      },
    catColor:{
        fontSize: 40,
    }
});

export default CatInfo;
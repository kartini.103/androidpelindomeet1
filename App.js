/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  Alert,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import CatInfo from './src/components/atom/CatInfo';

const App = () => {

  let catName = "Ndut";

  let catSpecies = {
    name: 'kucing item',
    warna: 'putih',
  };
  
  const alertPrevButton = () => {
    Alert.alert('Previous Cat');
  }

  const alertNextButton = () => {
    Alert.alert('Next Cat');
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
          <ScrollView>
            <Image
              source = {{uri : "https://icatcare.org/app/uploads/2018/07/Thinking-of-getting-a-cat.png" }}
              style = {{width: 400, height: 400}}
            />
            <CatInfo species={catSpecies}/>
            <Text style={styles.catName}>
              Name: {catName}
            </Text>
            <View style={styles.container}>
              <View style={{flex:1}}>
                <Button title="Previous" color="#f194ff" onPress={ () => alertPrevButton()}/>
              </View>
              <View style={{flex:1}}>
                <Button title="Next" color="#a095ff" onPress={ () => alertNextButton()}/>
              </View>
            </View>
          </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  catName:{
    fontSize:20,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
